Pod::Spec.new do |s|
  s.name             = 'Transactions'
  s.version          = '10.1.13'
  s.summary          = 'Transactions SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://gitlab.com/p4909/ios-transactions.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.vendored_frameworks = "TransactionSDK.xcframework"
  
  s.dependency 'AFNetworking', '~> 4.0'
  s.dependency 'ZIPFoundation', '~> 0.9'
  s.swift_versions = '5.0'

  # Skip this architecture to pass Pod validation since we removed the `arm64` simulator ARCH in order to use lipo later
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64', 'VALID_ARCHS' => 'x86_64 armv7 arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end