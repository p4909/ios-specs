Pod::Spec.new do |s|
  s.name             = 'Payments'
  s.version          = '10.2.3'
  s.summary          = 'Payments SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/jonat10/ios-payments.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.vendored_frameworks = "Payments.xcframework"
  
  s.dependency 'Transactions', '~> 10.1.3'
  s.dependency 'QPosHardware', '~> 6.0.5'
  s.dependency 'MagicPosHardware', '~> 6.0.6'
  s.swift_versions = '5.0'

    # Skip this architecture to pass Pod validation since we removed the `arm64` simulator ARCH in order to use lipo later
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64', 'VALID_ARCHS' => 'x86_64 armv7 arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end
