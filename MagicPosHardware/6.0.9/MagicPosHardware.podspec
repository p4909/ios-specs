Pod::Spec.new do |s|
  s.name             = 'MagicPosHardware'
  s.version          = '6.0.9'
  s.summary          = 'MagicPosHardware SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://gitlab.com/p4909/ios-magicpossdk.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.vendored_frameworks = "MagicPosHardware.xcframework"
  
  s.dependency 'Transactions', '~> 10.1.9'
  s.swift_versions = '5.0'
end
