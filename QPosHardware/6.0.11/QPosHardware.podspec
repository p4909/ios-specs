Pod::Spec.new do |s|
  s.name             = 'QPosHardware'
  s.version          = '6.0.11'
  s.summary          = 'QPosHardware SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://gitlab.com/p4909/ios-qpossdk.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.vendored_frameworks = "QPosHardware.xcframework"
  
  s.dependency 'Transactions', '~> 10.1.14'
  s.swift_versions = '5.0'

    # Skip this architecture to pass Pod validation since we removed the `arm64` simulator ARCH in order to use lipo later
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64', 'VALID_ARCHS' => 'x86_64 armv7 arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end
